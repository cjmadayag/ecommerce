<?php 
	require '../partials/template.php';

	function get_title(){
		"Add Contact";
	}

	function get_body_contents(){
		$userId = $_SESSION["user"]["id"];
	?>
		<h1 class="text-center py-5">Add Contact</h1>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-3">
					<form method="POST" action="../controllers/add-contact-process.php">
						<div class="form-group">
							<label>Contact</label>
							<input type="number" name="contact" class="form-control">
						</div>
						<div class="text-center">
							<input type="hidden" name="user_id" value="<?= $userId ?>">
							<button class="btn btn-info" type="submit">Add Contact</button>
						</div>
					</form>
				</div>
			</div>
		</div>


	<?php
	}

 ?>