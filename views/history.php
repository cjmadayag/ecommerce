<?php 
	require '../partials/template.php';

	function get_title(){
		"History";
	}

	function get_body_contents(){
	?>
		<div class="col-lg-6 offset-3">
			<table class="table">
				<thead>
					<tr>
						<td>Order ID</td>
						<td>Items</td>
						<td>Price</td>
					</tr>
				</thead>
				<tbody>
					<?php  
						require '../controllers/connection.php';
						$email = $_SESSION["user"]["email"];
						$orders = mysqli_query($conn,"select * from orders where user_id=(select id from users where email='$email')");

						foreach($orders as $order){
							$orderId = $order["id"];
						?>
							<tr>
								<td><?= $orderId ?></td>
								<td>
									<?php 
										$items = mysqli_query($conn,"select * from items where id in (select item_id from item_order where order_id=$orderId)");
		
										foreach ($items as $item){
											echo $item["name"]."<br>";
										}
									?>
								</td>
								<td><?= $order["total"] ?></td>
							</tr>
						<?php
						}

					?>
				</tbody>
			</table>
		</div>
	<?php
	}	
?>