<?php 
	require '../partials/template.php';

	function get_title(){
		echo "Transaction";
	}

	function get_body_contents(){
	?>
		<div class="col-lg-8 offset-2">
			<table class="table">
				<thead>
					<tr>
						<td>Order ID</td>
						<td>User ID</td>
						<td>Order Details</td>
						<td>Payment</td>
						<td>Status</td>
					</tr>
				</thead>
				<tbody>
					<?php 
						require '../controllers/connection.php';

						$orders = mysqli_query($conn,"select * from orders");

						foreach ($orders as $order) {
						?>
							<tr>
								<td><?= $order["id"] ?></td>
								<td><?= $order["user_id"] ?></td>
								<td>
									<?php 
										$orders = mysqli_query($conn,"select * from orders join item_order on (item_order.order_id = orders.id) join items on (items.id = item_order.item_id) where orders.id =".$order["id"]);

										foreach ($orders as $order) {
											echo $order["name"]."<br>";
										}
									?>
								</td>
								<td>
									<?php
									$paymentId = $order["payment_id"];
									$payment = mysqli_fetch_assoc(mysqli_query($conn,"select * from payments where id=$paymentId"));
									echo $payment["name"];
									?>
								</td>
								<td>
									<span class="spanStat">
										<?php
										$statusId = $order["status_id"];
										$status = mysqli_fetch_assoc(mysqli_query($conn,"select * from statuses where id=$statusId"));
										echo $status["name"];
										?>
									</span>
								</td>
							</tr>

						<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript" src="../assets/scripts/update-transaction-status.js"></script>
	<?php
	}
 ?>