<?php 
	require '../partials/template.php';

	function get_title(){
		echo "Profile";
	}

	function get_body_contents(){
		require '../controllers/connection.php';
	?>
		<h1 class="text-center py-5">Profile Page</h1>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 text-center">
					<h3>Profile</h3>
					<label>First Name</label>
					<h5><?= $_SESSION["user"]["firstName"] ?></h5>
					<label>Last Name</label>
					<h5><?= $_SESSION["user"]["lastname"] ?></h5>
					<label>Email Name</label>
					<h5><?= $_SESSION["user"]["email"] ?></h5>
				</div>
				<div class="col-lg-3">
					<h3>Addresses</h3>
					<ul>
					<?php
						$userId = $_SESSION["user"]["id"];
						$address_query="select * from addresses where user_id=$userId";
						$address = mysqli_query($conn, $address_query);

						foreach($address as $indiv_address){
						?>
							<li><?php echo $indiv_address["address1"] . " " . $indiv_address["address2"] . "<br>" . $indiv_address["city"] . " " . $indiv_address["zip_code"] ?></li>
						<?php
						}
					?>
					<a href="add-address.php" class="btn btn-info">Add new address</a>
					</ul>
				</div>
				<div class="col-lg-3">
					<h3>Contacts</h3>
					<ul>
					<?php
						$contact_query="select * from contacts where user_id=$userId";
						$contact = mysqli_query($conn, $contact_query);

						foreach($contact as $indiv_contact){
						?>
							<li><?php echo $indiv_contact["contactNo"] . "<br>"?></li>
						<?php
						}
					?>
					<a href="add-contact.php" class="btn btn-info">Add new contact</a>
					</ul>
				</div>
			</div>
		</div>



	<?php
	}
 ?>