<?php 
	require '../partials/template.php';

	function get_title(){
		"Add Address";
	}

	function get_body_contents(){
		$userId = $_SESSION["user"]["id"];
	?>
		<h1 class="text-center py-5">Add Address</h1>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-3">
					<form method="POST" action="../controllers/add-address-process.php">
						<div class="form-group">
							<label>Address 1</label>
							<input type="text" name="address1" class="form-control">
						</div>
						<div class="form-group">
							<label>Address 2</label>
							<input type="text" name="address2" class="form-control">
						</div>
						<div class="form-group">
							<label>City</label>
							<input type="text" name="city" class="form-control">
						</div>
						<div class="form-group">
							<label>Zip Code</label>
							<input type="text" name="zipCode" class="form-control">
						</div>
						<div class="text-center">
							<input type="hidden" name="user_id" value="<?= $userId ?>">
							<button class="btn btn-info" type="submit">Add Address</button>
						</div>
					</form>
				</div>
			</div>
		</div>


	<?php
	}

 ?>